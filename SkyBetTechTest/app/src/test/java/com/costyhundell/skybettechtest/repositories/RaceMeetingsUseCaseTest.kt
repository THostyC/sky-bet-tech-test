package com.costyhundell.skybettechtest.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.costyhundell.skybettechtest.MockHorseRacesApi
import com.costyhundell.skybettechtest.utils.HorseRacesApi
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

class RaceMeetingsUseCaseTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient())
        .baseUrl("http://test.com")
        .build()

    private val networkBehavior = NetworkBehavior.create()

    private val mockRetrofit = MockRetrofit.Builder(retrofit)
        .networkBehavior(networkBehavior)
        .build()


    private lateinit var raceMeetingsUseCase: RaceMeetingsUseCase

    @Before
    fun setUp() {

            val delegate = mockRetrofit.create(HorseRacesApi::class.java)
            val mockTorAlarmApi = MockHorseRacesApi(delegate)

            raceMeetingsUseCase = RaceMeetingsUseCase(mockTorAlarmApi, Schedulers.trampoline(), Schedulers.trampoline())
    }

    @Test
    fun testGetRaceListComplete() {
        raceMeetingsUseCase.getRaceList().test().assertComplete()
    }

    @Test
    fun testGetRaceListResult() {
        raceMeetingsUseCase.getRaceList().test().assertResult(emptyList())
    }

}