package com.costyhundell.skybettechtest

import com.costyhundell.skybettechtest.data_models.RaceDetails
import com.costyhundell.skybettechtest.data_models.RaceMeeting
import com.costyhundell.skybettechtest.data_models.RaceSummary
import com.costyhundell.skybettechtest.data_models.Ride
import com.costyhundell.skybettechtest.utils.HorseRacesApi
import io.reactivex.Single
import okhttp3.Call
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls

class MockHorseRacesApi(private val delegate: BehaviorDelegate<HorseRacesApi>): HorseRacesApi {
    override fun getRacesList(): Single<List<RaceMeeting>>
            = delegate.returning(Calls.response(emptyList<RaceMeeting>())).getRacesList()

    override fun getRaceDetails(id: Int): Single<RaceDetails>
            = delegate.returning(Calls.response(RaceDetails(RaceSummary(
        "Doncaster Derby", "Doncaster", "1m 4f 500yds",
        "2017-2-09", "13:45", 8, "WEIGHEDIN", "Good to soft", true, false, "test.com"
    ),
        emptyList()))).getRaceDetails(id)
}