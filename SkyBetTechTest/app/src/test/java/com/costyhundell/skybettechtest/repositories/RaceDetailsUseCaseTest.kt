package com.costyhundell.skybettechtest.repositories

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.costyhundell.skybettechtest.Constants
import com.costyhundell.skybettechtest.MockHorseRacesApi
import com.costyhundell.skybettechtest.data_models.RaceDetails
import com.costyhundell.skybettechtest.data_models.RaceSummary
import com.costyhundell.skybettechtest.mock
import com.costyhundell.skybettechtest.utils.HorseRacesApi
import com.costyhundell.skybettechtest.whenever
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertTrue
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.verify
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

class RaceDetailsUseCaseTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val sharedPreferences = mock<SharedPreferences>()
    private val mockEditor = mock<SharedPreferences.Editor>()


    private val raceSummary = RaceSummary(
        "Doncaster Derby", "Doncaster", "1m 4f 500yds",
        "2017-2-09", "13:45", 8, "WEIGHEDIN", "Good to soft", true, false, "test.com"
    )
    private var raceDetails = RaceDetails(raceSummary, emptyList())

    private val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient())
        .baseUrl("http://test.com")
        .build()

    private val networkBehavior = NetworkBehavior.create()

    private val mockRetrofit = MockRetrofit.Builder(retrofit)
        .networkBehavior(networkBehavior)
        .build()


    private lateinit var raceDetailsUseCase: RaceDetailsUseCase

    @Before
    fun setUp() {

        val delegate = mockRetrofit.create(HorseRacesApi::class.java)
        val mockTorAlarmApi = MockHorseRacesApi(delegate)

        whenever(sharedPreferences.edit()).thenReturn(mockEditor)
        whenever(mockEditor.putBoolean(Constants.IS_FRACTIONAL_KEY, false)).thenReturn(mockEditor)
        whenever(sharedPreferences.getBoolean(Constants.IS_FRACTIONAL_KEY, true)).thenReturn(true)


        raceDetailsUseCase = RaceDetailsUseCase(mockTorAlarmApi, Schedulers.trampoline(), Schedulers.trampoline(), sharedPreferences)
    }

    @Test
    fun testGetRaceDetailsComplete() {
        raceDetailsUseCase.getRaceDetails(1).test().assertComplete()
    }

    @Test
    fun testGetRaceDetailsResult() {
        raceDetailsUseCase.getRaceDetails(1).test().assertResult(raceDetails)
    }

    @Test
    fun testGetIsFractional() {
        assertTrue(raceDetailsUseCase.getIsFractional())
    }

    @Test
    fun testSetIsFractional() {
        raceDetailsUseCase.setIsFractional(false)
        verify(mockEditor).putBoolean(Constants.IS_FRACTIONAL_KEY, false)
    }
}