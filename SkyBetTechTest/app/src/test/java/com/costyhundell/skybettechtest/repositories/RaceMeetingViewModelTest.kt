package com.costyhundell.skybettechtest.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.costyhundell.skybettechtest.data_models.RaceMeeting
import com.costyhundell.skybettechtest.mock
import com.costyhundell.skybettechtest.utils.StateHandler
import com.costyhundell.skybettechtest.view_models.RaceMeetingsViewModel
import com.costyhundell.skybettechtest.whenever
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class RaceMeetingViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val raceMeetingsRepository = mock<RaceMeetingsRepository>()
    private val observer = mock<Observer<StateHandler>>()

    private val raceMeeting = RaceMeeting("Steeple Chase", "Beverley", "1m 1f 100yds", "2019-06-03",
        "13:05", 10, "Good to Soft", false, 1)

    private val raceMeetingsViewModel by lazy {
        RaceMeetingsViewModel(
            raceMeetingsRepository
        )
    }

    @Test
    fun getRaceMeetingsCompletes() {
        whenever(raceMeetingsRepository.getRaceList()).thenReturn(Single.just(emptyList()))

        raceMeetingsViewModel.raceResponse.test().assertComplete()
    }

    @Test
    fun getRaceMeetingsAsExpected() {
        val response = arrayListOf(raceMeeting)
        whenever(raceMeetingsRepository.getRaceList())
            .thenReturn(Single.just(response))

        raceMeetingsViewModel.raceResponse
            .test()
            .assertValue(response)
    }

    @Test
    fun getRaceMeetingsLiveData() {
        val response = listOf(raceMeeting)
        whenever(raceMeetingsRepository.getRaceList())
            .thenReturn(Single.just(response))

        raceMeetingsViewModel.getRaces().observeForever {
            assert(it == response)
        }
        raceMeetingsViewModel.loadRaceResponse()
    }

    @Test
    fun getStateLiveData() {
        val response = listOf(raceMeeting)
        whenever(raceMeetingsRepository.getRaceList())
            .thenReturn(Single.just(response))


        raceMeetingsViewModel.getState().observeForever(observer)
        raceMeetingsViewModel.loadRaceResponse()
        val argumentCaptor = ArgumentCaptor.forClass(StateHandler::class.java)
        argumentCaptor.run {
            verify(observer, times(1)).onChanged(capture())
            val ( successState) = allValues

            assertEquals(StateHandler.success().state, successState.state)
        }
    }

    @Test
    fun testRefresh() {
        val response = listOf(raceMeeting)
        whenever(raceMeetingsRepository.getRaceList())
            .thenReturn(Single.just(response))


        raceMeetingsViewModel.getState().observeForever(observer)
        raceMeetingsViewModel.refresh()
        val argumentCaptor = ArgumentCaptor.forClass(StateHandler::class.java)
        argumentCaptor.run {
            verify(observer, times(2)).onChanged(capture())
            val (loadingState, successState) = allValues

            assertEquals(StateHandler.success().state, successState.state)
            assertEquals(StateHandler.loading().state, loadingState.state)

        }
    }
}