package com.costyhundell.skybettechtest.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.costyhundell.skybettechtest.data_models.HorseDetails
import com.costyhundell.skybettechtest.data_models.RaceDetails
import com.costyhundell.skybettechtest.data_models.RaceSummary
import com.costyhundell.skybettechtest.data_models.Ride
import com.costyhundell.skybettechtest.mock
import com.costyhundell.skybettechtest.utils.StateHandler
import com.costyhundell.skybettechtest.utils.viewModelModule
import com.costyhundell.skybettechtest.view_models.RaceDetailsViewModel
import com.costyhundell.skybettechtest.whenever
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertFalse
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class RaceDetailsViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val raceDetailsRepository = mock<RaceDetailsRepository>()
    private val observer = mock<Observer<StateHandler>>()
    private val raceSummary = RaceSummary(
        "Doncaster Derby", "Doncaster", "1m 4f 500yds",
        "2017-2-09", "13:45", 8, "WEIGHEDIN", "Good to soft", true, false, "test.com"
    )
    private var raceDetails = RaceDetails(raceSummary, emptyList())

    private val horseDetails = HorseDetails("Blue", 24, "2011-04-09", "f")

    private val unsortedList = listOf(
        Ride(2, "", horseDetails, "12346", "9-6", "3/1"),
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1"),
        Ride(3, "", horseDetails, "21246", "9-6", "1/3")
    )

    private val sortedListByNumberAsc = listOf(
        Ride(2, "", horseDetails, "12346", "9-6", "3/1"),
        Ride(3, "", horseDetails, "21246", "9-6", "1/3"),
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1")
    )

    private val sortedListByNumberDesc = listOf(
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1"),
        Ride(3, "", horseDetails, "21246", "9-6", "1/3"),
        Ride(2, "", horseDetails, "12346", "9-6", "3/1")
    )

    private val sortedByOddsAsc = listOf(
        Ride(3, "", horseDetails, "21246", "9-6", "1/3"),
        Ride(2, "", horseDetails, "12346", "9-6", "3/1"),
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1")
    )

    private val sortedByOddsDesc = listOf(
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1"),
        Ride(2, "", horseDetails, "12346", "9-6", "3/1"),
        Ride(3, "", horseDetails, "21246", "9-6", "1/3")
    )

    private val sortedByFormAsc = listOf(
        Ride(3, "", horseDetails, "21246", "9-6", "1/3"), // 3
        Ride(2, "", horseDetails, "12346", "9-6", "3/1"), // 3.2
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1") //4.5
    )

    private val sortedByFormDesc = listOf(
        Ride(5, "", horseDetails, "1034-", "9-6", "7/1"), //4.5
        Ride(2, "", horseDetails, "12346", "9-6", "3/1"), // 3.2
        Ride(3, "", horseDetails, "21246", "9-6", "1/3") // 3

    )

    private val ride = Ride(5, "", horseDetails, "1034-", "9-6", "11/8")
    private val rideWithOutDash = Ride(5, "", horseDetails, "18347", "9-6", "11/8")

    private val expectedRideForm = 4.50f
    private val expectedRideNoDashForm = 4.6f

    private val expectedDecimal = 2.38f
    private val raceDetailsViewModel by lazy {
        RaceDetailsViewModel(
            raceDetailsRepository
        )
    }

    @Test
    fun getRaceDetailsAsExpected() {
        whenever(raceDetailsRepository.getRaceDetails(1)).thenReturn(Single.just(raceDetails))

        raceDetailsViewModel.getRaceDetails().observeForever {
            assert(it == raceSummary)
        }

        raceDetailsViewModel.loadRiders(1)

    }

    @Test
    fun getRaceListAsExpected() {

        whenever(raceDetailsRepository.getRaceDetails(1)).thenReturn(Single.just(raceDetails))

        raceDetailsViewModel.getRiders().observeForever {
            assert(it == emptyList<Ride>())
        }

        raceDetailsViewModel.loadRiders(1)

    }

    @Test
    fun testSortByClothNumberAscending() {
        raceDetailsViewModel.getSortedList().observeForever {
            assert(it == sortedListByNumberAsc)
        }

        raceDetailsViewModel.sortByClothAsc(unsortedList)

    }

    @Test
    fun testSortByClothNumberDescending() {
        raceDetailsViewModel.getSortedList().observeForever {
            assert(it == sortedListByNumberDesc)
        }
        raceDetailsViewModel.sortByClothDesc(unsortedList)
    }

    @Test
    fun testSortByOddsAscending() {
        raceDetailsViewModel.getSortedList().observeForever {
            assert(it == sortedByOddsAsc)
        }
        raceDetailsViewModel.sortByOddsAsc(unsortedList)
    }

    @Test
    fun testSortByOddsDescending() {
        raceDetailsViewModel.getSortedList().observeForever {
            assert(it == sortedByOddsDesc)
        }
        raceDetailsViewModel.sortByOddsDesc(unsortedList)
    }

    @Test
    fun testConvertOddsToDecimal() {
        assert(expectedDecimal == raceDetailsViewModel.convertFractionToDecimal(ride))
    }

    @Test
    fun testGetAverageForm() {
        assert(expectedRideForm == raceDetailsViewModel.getAverageForm(ride))
        assert(expectedRideNoDashForm == raceDetailsViewModel.getAverageForm(rideWithOutDash))
    }

    @Test
    fun testSortByFormAscending() {
        raceDetailsViewModel.getSortedList().observeForever {
            assert(it == sortedByFormAsc)
        }
        raceDetailsViewModel.sortByFormAsc(unsortedList)
    }

    @Test
    fun testSortByFormDescending() {
        raceDetailsViewModel.getSortedList().observeForever {
            assert(it == sortedByFormDesc)
        }
        raceDetailsViewModel.sortByFormDesc(unsortedList)
    }

    @Test
    fun getStateLiveData() {
        whenever(raceDetailsRepository.getRaceDetails(1))
            .thenReturn(Single.just(raceDetails))

        //Needed to allow state to be called in this viewModel
        raceDetailsViewModel.getRiders().observeForever {  }
        raceDetailsViewModel.getState().observeForever(observer)
        raceDetailsViewModel.loadRiders(1)
        val argumentCaptor = ArgumentCaptor.forClass(StateHandler::class.java)
        argumentCaptor.run {
            verify(observer, times(1)).onChanged(capture())
            val ( successState) = allValues

            assertEquals(StateHandler.success().state, successState.state)
        }
    }

    @Test
    fun testRefresh() {
        whenever(raceDetailsRepository.getRaceDetails(1))
            .thenReturn(Single.just(raceDetails))


        raceDetailsViewModel.getRiders().observeForever {  }
        raceDetailsViewModel.getState().observeForever(observer)
        raceDetailsViewModel.loadRiders(1)
        raceDetailsViewModel.refresh()
        val argumentCaptor = ArgumentCaptor.forClass(StateHandler::class.java)
        argumentCaptor.run {
            verify(observer, times(3)).onChanged(capture())
            val (initialSuccess, loadingState, successState) = allValues

            assertEquals(StateHandler.success().state, initialSuccess.state)
            assertEquals(StateHandler.success().state, successState.state)
            assertEquals(StateHandler.loading().state, loadingState.state)

        }
    }


    @Test
    fun testSetIsFractional() {
        raceDetailsViewModel.setIsFractional(true)
        verify(raceDetailsRepository).setIsFractional(true)
    }

    @Test
    fun testGetIsFractional() {
        whenever(raceDetailsRepository.getIsFractional()).thenReturn(false)
        assertFalse(raceDetailsViewModel.getIsFractional())
    }


}