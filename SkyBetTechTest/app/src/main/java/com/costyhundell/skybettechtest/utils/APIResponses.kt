package com.costyhundell.skybettechtest.utils

val raceMeetingsResponse = """[
  {
    "name": "Maiden Plate (F & M)",
    "course_name": "Vaal",
    "distance": "4f 214y",
    "date": "2017-12-15",
    "time": "10:45",
    "ride_count": 8,
    "going": "Good to Soft",
    "has_handicap": false,
    "race_id": 1
  },
  {
    "name": "40 Guinaes",
    "course_name": "Doncaster",
    "distance": "4f 214y",
    "date": "2017-12-15",
    "time": "11:25",
    "ride_count": 7,
    "going": "Good to Firm",
    "has_handicap": false,
    "race_id": 2
  },
  {
    "name": "Sky Bet Junior Hurdles",
    "course_name": "Ascot",
    "distance": "1m 4f",
    "date": "2017-12-15",
    "time": "12:25",
    "ride_count": 15,
    "going": "Good",
    "has_handicap": false,
    "race_id": 3
  },
  {
    "name": "NatEast Sprint",
    "course_name": "Pontefract",
    "distance": "6f",
    "date": "2017-12-15",
    "time": "12:55",
    "ride_count": 6,
    "going": "Soft",
    "has_handicap": false,
    "race_id": 4
  },{
    "name": "Visit Yorkshire Chase",
    "course_name": "Wetherby",
    "distance": "2m 1f 5y",
    "date": "2017-12-15",
    "time": "13:15",
    "ride_count": 11,
    "going": "Good, Good to Firm in Places",
    "has_handicap": false,
    "race_id": 5
  },{
    "name": "Champions Sprint",
    "course_name": "Ripon",
    "distance": "7f 100y",
    "date": "2017-12-15",
    "time": "13:55",
    "ride_count": 4,
    "going": "Good to Soft",
    "has_handicap": false,
    "race_id": 6
  },{
    "name": "Run Run as Fast As you Can",
    "course_name": "Thirsk",
    "distance": "1m",
    "date": "2017-12-15",
    "time": "14:25",
    "ride_count": 9,
    "going": "Firm",
    "has_handicap": false,
    "race_id": 7
  },{
    "name": "Comedy Store Chase",
    "course_name": "Redcar",
    "distance": "1m 6f 25y",
    "date": "2017-12-15",
    "time": "14:55",
    "ride_count": 5,
    "going": "Good to Soft",
    "has_handicap": false,
    "race_id": 8
  }, {
    "name": "The Bland National",
    "course_name": "Aintree",
    "distance": "4m",
    "date": "2017-12-15",
    "time": "15:20",
    "ride_count": 8,
    "going": "Good to Soft",
    "has_handicap": false,
    "race_id": 9
  }, {
    "name": "Endgame",
    "course_name": "Chester",
    "distance": "6f",
    "date": "2017-12-15",
    "time": "15:50",
    "ride_count": 5,
    "going": "Good to Firm",
    "has_handicap": false,
    "race_id": 10
  }
]
"""

val race1Response = """{
    "race_summary": {
        "name": "Maiden Plate (F & M)",
        "course_name": "Vaal",
        "age": "3YO plus",
        "distance": "4f 214y",
        "date": "2017-12-15",
        "time": "10:45",
        "ride_count": 8,
        "race_stage": "WEIGHEDIN",
        "going": "Good to Soft",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2Fvaal.png?alt=media&token=07af98ff-11fd-4cca-a839-c1e8fb3b0cb8"
    },
    "rides": [
        {
            "cloth_number": 5,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Cape Orchid",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "412-12",
            "handicap": "9-6",
            "current_odds": "5/2"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Osculation",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "325433",
            "handicap": "9-6",
            "current_odds": "13/8"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Casa De Var",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "48-8191",
            "handicap": "9-6",
            "current_odds": "11/1"
        },
        {
            "cloth_number": 8,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Madam Secretary",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "226222",
            "handicap": "9-6",
            "current_odds": "17/2"
        },
        {
            "cloth_number": 2,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Music Maker",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "7467-4",
            "handicap": "9-6",
            "current_odds": "14/2"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Twinkle Town",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "3461",
            "handicap": "9-6",
            "current_odds": "14/2"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#B8BEFF",
            "horse": {
                "name": "Brighton Belle",
                "last_ran_days": 18,
                "foaled": "2013-11-06",
                "sex": "f"
                },
            "formsummary": "57687-",
            "withdrawn" : true,
            "handicap": "9-6",
            "current_odds": "33/1"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#A7E8D8",
            "horse": {
                "name": "Vivacious Vivian",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "0-6",
            "handicap": "9-6",
            "current_odds": "28/1"
        }
    ]
}
"""

val race2Response = """{
    "race_summary": {
        "name": "40 Guinaes",
        "course_name": "Doncaster",
        "age": "8YO plus",
        "distance": "1m 12f 214y",
        "date": "2017-12-15",
        "time": "11:25",
        "ride_count": 7,
        "race_stage": "WEIGHEDIN",
        "going": "Good to Firm",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2Fdoncaster%20race%20course.png?alt=media&token=6f067730-be6d-4386-9180-26478ca010d6"
    },
    "rides": [
        {
            "cloth_number": 1,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Dessert Sun",
                "days_since_last_run": 24,
                "foaled": "2010-09-28",
                "sex": "f"
            },
            "formsummary": "312111",
            "handicap": "9-6",
            "current_odds": "1/2"
        },
        {
            "cloth_number": 2,
            "cloth_colour": "#EEF9B8",
            "horse": {
                "name": "Pontiac Bandit",
                "days_since_last_run": 42,
                "foaled": "2009-09-17",
                "sex": "f"
            },
            "formsummary": "388-33",
            "handicap": "9-6",
            "current_odds": "13/1"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#A8FFBD",
            "horse": {
                "name": "Casa Amore",
                "days_since_last_run": 28,
                "foaled": "2010-1-21",
                "sex": "m"
                },
            "formsummary": "080039",
            "handicap": "9-6",
            "current_odds": "150/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#B89FF9",
            "horse": {
                "name": "Principled Skinner",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "m"
                },
            "formsummary": "433251",
            "handicap": "9-6",
            "current_odds": "17/2"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#FF44B8",
            "horse": {
                "name": "Salt and Pferde",
                "last_ran_days": 35,
                "foaled": "2011-09-25",
                "sex": "f"
                },
            "formsummary": "102368",
            "handicap": "9-6",
            "current_odds": "6/1"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#DD7468",
            "horse": {
                "name": "Knight Mare",
                "last_ran_days": 35,
                "foaled": "2010-09-30",
                "sex": "m"
                },
            "formsummary": "546-14",
            "handicap": "9-6",
            "current_odds": "15/2"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#B8CEFF",
            "horse": {
                "name": "CinderStella",
                "last_ran_days": 18,
                "foaled": "2008-1-26",
                "sex": "f"
                },
            "formsummary": "1538-0",
            "handicap": "9-6",
            "current_odds": "42/1"
        }
    ]
}
"""

val race3Response = """{
    "race_summary": {
        "name": "Sky Bet Junior Hurdles",
        "course_name": "Ascot",
        "age": "3YO plus",
        "distance": "1m 4f",
        "date": "2017-12-15",
        "time": "12:25",
        "ride_count": 15,
        "race_stage": "WEIGHEDIN",
        "going": "Good",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FAscot_map_turf_flat.png?alt=media&token=29e0525b-48f8-495a-8ed0-5c9f4cc59c49"
    },
    "rides": [
        {
            "cloth_number": 5,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "El Loco",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "112",
            "handicap": "9-6",
            "current_odds": "3/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Hard Rock Falling",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "333",
            "handicap": "9-6",
            "current_odds": "7/2"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Destinys Adult",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "490",
            "handicap": "9-6",
            "current_odds": "34/1"
        },
        {
            "cloth_number": 8,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Sacapuntas",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "-1",
            "handicap": "9-6",
            "current_odds": "17/2"
        },
        {
            "cloth_number": 2,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Square Root",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "4",
            "handicap": "9-6",
            "current_odds": "14/1"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Duck and Cover",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "633",
            "handicap": "9-6",
            "current_odds": "9/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#B8BEFF",
            "horse": {
                "name": "4gone Conclusion",
                "last_ran_days": 18,
                "foaled": "2013-11-06",
                "sex": "f"
                },
            "formsummary": "8",
            "withdrawn" : true,
            "handicap": "9-6",
            "current_odds": "50/1"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#A7E8D8",
            "horse": {
                "name": "Stir the Pot",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "06",
            "handicap": "9-6",
            "current_odds": "19/1"
        },
        {
            "cloth_number": 15,
            "cloth_colour": "#A700D8",
            "horse": {
                "name": "Mind your Own Beeswax",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "-",
            "handicap": "9-6",
            "current_odds": "100/1"
        },
        {
            "cloth_number": 9,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Punny",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "33",
            "handicap": "9-6",
            "current_odds": "7/1"
        },
        {
            "cloth_number": 14,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Domestic Treble",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "04",
            "handicap": "9-6",
            "current_odds": "32/1"
        },
        {
            "cloth_number": 10,
            "cloth_colour": "#DD7468",
            "horse": {
                "name": "Briggate",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "23",
            "handicap": "9-6",
            "current_odds": "11/1"
        },
        {
            "cloth_number": 13,
            "cloth_colour": "#EFEFEF",
            "horse": {
                "name": "Starlord",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "122",
            "handicap": "9-6",
            "current_odds": "5/1"
        },
        {
            "cloth_number": 11,
            "cloth_colour": "#0000EE",
            "horse": {
                "name": "Six of 2",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "32",
            "handicap": "9-6",
            "current_odds": "13/1"
        }
        ,
        {
            "cloth_number": 12,
            "cloth_colour": "#0FF000",
            "horse": {
                "name": "Family Business",
                "last_ran_days": 14,
                "foaled": "2013-10-24",
                "sex": "m"
                },
            "formsummary": "9090",
            "handicap": "9-6",
            "current_odds": "120/1"
        }
    ]
}
"""

val race4Response = """{
    "race_summary": {
        "name": "NatEast Sprint",
        "course_name": "Pontefract",
        "age": "3YO plus",
        "distance": "6f",
        "date": "2017-12-15",
        "time": "12:55",
        "ride_count": 6,
        "race_stage": "WEIGHEDIN",
        "going": "Soft",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FAscot_map_turf_flat.png?alt=media&token=29e0525b-48f8-495a-8ed0-5c9f4cc59c49"
    },
    "rides": [
        {
            "cloth_number": 3,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Bacon face",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "0889",
            "handicap": "9-6",
            "current_odds": "30/1"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Irish Kaffee",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "221",
            "handicap": "9-6",
            "current_odds": "10/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "You Sane Bolt",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "11121",
            "handicap": "9-6",
            "current_odds": "1/1"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Boy and Barrel",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "880-51",
            "handicap": "9-6",
            "current_odds": "31/3"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Butcher of Beeston",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "00966",
            "handicap": "9-6",
            "current_odds": "28/1"
        },
        {
            "cloth_number": 2,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Walk the Line",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "301-11",
            "handicap": "9-6",
            "current_odds": "8/1"
        }
    ]
}
"""

val race5Response = """{
    "race_summary": {
        "name": "Visit Yorkshire Chase",
        "course_name": "Wetherby",
        "age": "3YO plus",
        "distance": "2m 1f 5y",
        "date": "2017-12-15",
        "time": "13:15",
        "ride_count": 11,
        "race_stage": "WEIGHEDIN",
        "going": "Good, Good to Firm in Places",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FWetherby_map_turf_flat.png?alt=media&token=052a3185-aa4a-4751-b9a0-3138d9f7b3d2"
    },
    "rides": [
        {
            "cloth_number": 3,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Sadie's Regret",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "4-476",
            "handicap": "9-6",
            "current_odds": "24/1"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Bojack",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "0-090",
            "handicap": "9-6",
            "current_odds": "22/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Dallimory",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "34332",
            "handicap": "9-6",
            "current_odds": "7/1"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Toby Jugg",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "232-1",
            "handicap": "9-6",
            "current_odds": "4/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "The Square Horse",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "00966",
            "handicap": "9-6",
            "current_odds": "36/1"
        },
        {
            "cloth_number": 2,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Grubber Through",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "345-41",
            "handicap": "9-6",
            "current_odds": "17/1"
        },
        {
            "cloth_number": 9,
            "cloth_colour": "#B8CEFF",
            "horse": {
                "name": "Slip and Bide",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "111-21",
            "handicap": "9-6",
            "current_odds": "1/2"
        },
        {
            "cloth_number": 11,
            "cloth_colour": "#EE00FF",
            "horse": {
                "name": "Wall Clock",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "05-594",
            "handicap": "9-6",
            "current_odds": "45/2"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Tottenham Hot Spurts",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "553-21",
            "handicap": "9-6",
            "current_odds": "11/1"
        },
        {
            "cloth_number": 10,
            "cloth_colour": "#DD45EE",
            "horse": {
                "name": "Destroy and Exit",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "999-04",
            "handicap": "9-6",
            "current_odds": "8/3"
        },
        {
            "cloth_number": 8,
            "cloth_colour": "#B8CEFF",
            "horse": {
                "name": "Ronny Hotdogs",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "44478",
            "handicap": "9-6",
            "current_odds": "15/4"
        }

    ]
}
"""

val race6Response = """{
    "race_summary": {
        "name": "Champions Sprint",
        "course_name": "Ripon",
        "age": "3YO plus",
        "distance": "7f 100y",
        "date": "2017-12-15",
        "time": "13:55",
        "ride_count": 4,
        "race_stage": "WEIGHEDIN",
        "going": "Good to Soft",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FRipon_map_turf_flat.png?alt=media&token=2966fef4-f0f3-40d9-b0a0-bd929c292a22"
    },
    "rides": [
        {
            "cloth_number": 3,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "I'm Ronnie Pickering",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "4-476",
            "handicap": "9-6",
            "current_odds": "24/1"
        },
        {
            "cloth_number": 2,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Berliner Luft",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "0-090",
            "handicap": "9-6",
            "current_odds": "22/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Killepitsche",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "34332",
            "handicap": "9-6",
            "current_odds": "7/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "One Klich is all it takes",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "232-1",
            "handicap": "9-6",
            "current_odds": "4/1"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Off the Mark",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "232-1",
            "handicap": "9-6",
            "current_odds": "NR"
        }

    ]
}
"""

val race7Response = """{
    "race_summary": {
        "name": "Run Run as Fast As you Can",
        "course_name": "Thirsk",
        "age": "3YO plus",
        "distance": "1m",
        "date": "2017-12-15",
        "time": "14:25",
        "ride_count": 9,
        "race_stage": "WEIGHEDIN",
        "going": "Firm",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FThirsk_map_turf_flat.png?alt=media&token=85e06ef1-a190-43ca-a4dc-6c385734fb3b"
    },
    "rides": [
        {
            "cloth_number": 2,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Daniels Salsa",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "00000",
            "handicap": "9-6",
            "current_odds": "80/1"
        },
        {
            "cloth_number": 9,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Nigels Mirage",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "489-0",
            "handicap": "9-6",
            "current_odds": "32/1"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Scrambled",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "11123",
            "handicap": "9-6",
            "current_odds": "4/1"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Drieundneinzig",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "3-221",
            "handicap": "9-6",
            "current_odds": "5/1"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Dad's Best Friend",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "44563",
            "handicap": "9-6",
            "current_odds": "14/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Bubba Gump",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "88248",
            "handicap": "9-6",
            "current_odds": "9/1"
        },
        {
            "cloth_number": 8,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Jonny Däpp",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "2221-1",
            "handicap": "9-6",
            "current_odds": "10/1"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Vamos Leeds Carajo",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "333333",
            "handicap": "9-6",
            "current_odds": "3/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Bill Ayling",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "45670",
            "handicap": "9-6",
            "current_odds": "19/1"
        }
    ]
}
"""

val race8Response = """{
    "race_summary": {
        "name": "Comedy Store Chase",
        "course_name": "Redcar",
        "age": "3YO plus",
        "distance": "1m 6f 25y",
        "date": "2017-12-15",
        "time": "14:55",
        "ride_count": 5,
        "race_stage": "WEIGHEDIN",
        "going": "Good to Soft",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FRedcar_map_turf_flat.png?alt=media&token=c504dc07-e966-44d1-a558-c80874ed99da"
    },
    "rides": [
        {
            "cloth_number": 2,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Knock Knock",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "00000",
            "handicap": "9-6",
            "current_odds": "80/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Who's There",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "489-0",
            "handicap": "9-6",
            "current_odds": "32/1"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Boo",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "11123",
            "handicap": "9-6",
            "current_odds": "NR"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Boo Who?",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "3-221",
            "handicap": "9-6",
            "current_odds": "5/1"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Sorry",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "44563",
            "handicap": "9-6",
            "current_odds": "14/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#FF0068",
            "horse": {
                "name": "Didn't mean to",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "88248",
            "handicap": "9-6",
            "current_odds": "9/1"
        },
        {
            "cloth_number": 8,
            "cloth_colour": "#00E4B8",
            "horse": {
                "name": "Scare You",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "2221-1",
            "handicap": "9-6",
            "current_odds": "NR"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "ROFL Good One",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "333333",
            "handicap": "9-6",
            "current_odds": "NR"
        }
    ]
}
"""

val race9Response = """{
    "race_summary": {
        "name": "The Bland National",
        "course_name": "Aintree",
        "age": "3YO plus",
        "distance": "4m",
        "date": "2017-12-15",
        "time": "15:20",
        "ride_count": 8,
        "race_stage": "WEIGHEDIN",
        "going": "Good to Soft",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FAintree_map_turf_hurdle.png?alt=media&token=d0483bd6-1d5b-4b1e-bf0a-33f707604490"
    },
    "rides": [
        {
            "cloth_number": 2,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Brown Pasta",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "1111",
            "handicap": "9-6",
            "current_odds": "2/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Brown Bread",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "229-0",
            "handicap": "9-6",
            "current_odds": "13/1"
        },
        {
            "cloth_number": 6,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Ready Salted Crisps",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "23462",
            "handicap": "9-6",
            "current_odds": "8/1"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Porridge",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "3-111",
            "handicap": "9-6",
            "current_odds": "7/2"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "Jimmy Milner",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "11111",
            "handicap": "9-6",
            "current_odds": "1/10"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#FF0068",
            "horse": {
                "name": "Beige Walls",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "87258",
            "handicap": "9-6",
            "current_odds": "19/1"
        },
        {
            "cloth_number": 8,
            "cloth_colour": "#00E4B8",
            "horse": {
                "name": "Grey Sky",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "2-2415",
            "handicap": "9-6",
            "current_odds": "5/1"
        },
        {
            "cloth_number": 7,
            "cloth_colour": "#FF7468",
            "horse": {
                "name": "Jacket Potato",
                "last_ran_days": 35,
                "foaled": "2013-09-30",
                "sex": "m"
                },
            "formsummary": "2524",
            "handicap": "9-6",
            "current_odds": "8/1"
        }
    ]
}
"""

val race10Response = """{
    "race_summary": {
        "name": "Endgame",
        "course_name": "Chester",
        "age": "3YO plus",
        "distance": "6f",
        "date": "2017-12-15",
        "time": "15:50",
        "ride_count": 5,
        "race_stage": "WEIGHEDIN",
        "going": "Good to Firm",
        "has_handicap": false,
        "hidden": false,
        "course_url": "https://firebasestorage.googleapis.com/v0/b/sky-bet-tech-test-df32d.appspot.com/o/race_courses%2FChester_map_turf_flat.png?alt=media&token=093e1a7f-b974-4268-9167-ab0f1d4e2017"
    },
    "rides": [
        {
            "cloth_number": 2,
            "cloth_colour": "#E8FFC4",
            "horse": {
                "name": "Thanos",
                "days_since_last_run": 24,
                "foaled": "2013-09-28",
                "sex": "f"
            },
            "formsummary": "32412",
            "handicap": "9-6",
            "current_odds": "5/1"
        },
        {
            "cloth_number": 1,
            "cloth_colour": "#FFF9B8",
            "horse": {
                "name": "Iron Horse",
                "days_since_last_run": 19,
                "foaled": "2013-09-17",
                "sex": "f"
            },
            "formsummary": "0049-1",
            "handicap": "9-6",
            "current_odds": "15/1"
        },
        {
            "cloth_number": 4,
            "cloth_colour": "#B8FFBD",
            "horse": {
                "name": "Captain A-Mare-rica",
                "days_since_last_run": 28,
                "foaled": "2013-10-21",
                "sex": "f"
                },
            "formsummary": "-3468",
            "handicap": "9-6",
            "current_odds": "9/1"
        },
        {
            "cloth_number": 5,
            "cloth_colour": "#B8FFF9",
            "horse": {
                "name": "Hulk",
                "last_ran_days": 21,
                "foaled": "2013-08-08",
                "sex": "f"
                },
            "formsummary": "3-5642",
            "handicap": "9-6",
            "current_odds": "15/2"
        },
        {
            "cloth_number": 3,
            "cloth_colour": "#FFE4B8",
            "horse": {
                "name": "I am Groot",
                "last_ran_days": 35,
                "foaled": "2012-09-25",
                "sex": "m"
                },
            "formsummary": "0983-1",
            "handicap": "9-6",
            "current_odds": "23/10"
        }
    ]
}
"""