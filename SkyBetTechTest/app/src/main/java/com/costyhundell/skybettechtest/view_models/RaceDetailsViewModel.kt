package com.costyhundell.skybettechtest.view_models

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.costyhundell.skybettechtest.data_models.RaceDetails
import com.costyhundell.skybettechtest.data_models.RaceSummary
import com.costyhundell.skybettechtest.data_models.Ride
import com.costyhundell.skybettechtest.repositories.RaceDetailsRepository
import com.costyhundell.skybettechtest.utils.StateHandler
import com.costyhundell.skybettechtest.utils.Utils

class RaceDetailsViewModel(private val repository: RaceDetailsRepository) : ViewModel(), LifecycleOwner {
    override fun getLifecycle(): LifecycleRegistry = LifecycleRegistry(this)

    private val intLiveData = MutableLiveData<Int>()
    private val state = MutableLiveData<StateHandler>()
    private var refreshValue = 0

    private val raceDetails = Transformations.switchMap(intLiveData) { id ->
        MutableLiveData<RaceDetails>().apply {
            repository.getRaceDetails(id).subscribe { response ->
                if (response != null) {
                    state.postValue(StateHandler(StateHandler.Companion.State.SUCCESS, null))
                    this.postValue(response)
                } else {
                    state.postValue(StateHandler.error(Throwable("No data")))
                }
            }
        }
    }

    private val raceSummary = Transformations.switchMap(raceDetails) { details ->
        MutableLiveData<RaceSummary>().apply {
            this.postValue(details.summary)
        }
    }

    private val riderList = Transformations.switchMap(raceDetails) { details ->
        MutableLiveData<List<Ride>>().apply {
            this.postValue(details.rides)
        }
    }

    private val sortedList = MutableLiveData<List<Ride>>()

    fun loadRiders(id: Int) {
        refreshValue = id
        intLiveData.postValue(id)
    }

    fun getRiders(): LiveData<List<Ride>> = riderList

    fun getSortedList(): LiveData<List<Ride>> = sortedList

    fun getRaceDetails(): LiveData<RaceSummary> = raceSummary

    fun getState(): LiveData<StateHandler> = state

    fun sortByClothAsc() {
        sortByClothAsc(riderList.value)
    }

    fun sortByClothDesc() {
        sortByClothDesc(riderList.value)
    }

    @VisibleForTesting
    fun sortByClothAsc(list: List<Ride>?) {
        sortedList.postValue(list?.sortedBy {
            it.number
        })
    }

    @VisibleForTesting
    fun sortByClothDesc(list: List<Ride>?) {
        sortedList.postValue(list?.sortedByDescending {
            it.number
        })
    }

    fun sortByOddsAsc() {
        sortByOddsAsc(riderList.value)
    }

    @VisibleForTesting
    fun sortByOddsAsc(list: List<Ride>?) {
        val nonRunnerList = mutableListOf<Ride>()
        list?.forEach {
            if (it.odds == "NR")
                nonRunnerList.add(it)
        }
        val orderedList: MutableList<Ride>? = list?.filter {
            it.odds != "NR"
        }?.sortedBy {
            convertFractionToDecimal(it)
        }?.toMutableList()
        orderedList?.addAll(nonRunnerList)
        sortedList.postValue(orderedList)
    }

    fun sortByOddsDesc() {
        sortByOddsDesc(riderList.value)
    }

    @VisibleForTesting
    fun sortByOddsDesc(list: List<Ride>?) {
        val nonRunnerList = mutableListOf<Ride>()
        list?.forEach {
            if (it.odds == "NR")
                nonRunnerList.add(it)
        }
        val orderedList: MutableList<Ride>? = list?.filter {
            it.odds != "NR"
        }?.sortedByDescending {
            convertFractionToDecimal(it)
        }?.toMutableList()
        orderedList?.addAll(nonRunnerList)
        sortedList.postValue(orderedList)
    }

    fun sortByFormAsc() {
        sortByFormAsc(riderList.value)
    }

    @VisibleForTesting
    fun sortByFormAsc(list: List<Ride>?) {
        sortedList.postValue(list?.sortedBy {
            getAverageForm(it)
        })
    }

    fun setIsFractional(isFractional: Boolean) = repository.setIsFractional(isFractional)

    fun getIsFractional() = repository.getIsFractional()

    fun sortByFormDesc() {
        sortByFormDesc(riderList.value)
    }

    @VisibleForTesting
    fun sortByFormDesc(list: List<Ride>?) {
        sortedList.postValue(list?.sortedByDescending {
            getAverageForm(it)
        })
    }

    @VisibleForTesting
    fun convertFractionToDecimal(ride: Ride): Float = Utils.convertToDecimal(ride.odds)

    @VisibleForTesting
    fun getAverageForm(ride: Ride): Float = ride.form.toCharArray().let { formArray ->
        val formList = mutableListOf<Int>()
        formArray.forEach { formValue ->
            when (val formString = Character.toString(formValue)) {
                "0" -> formList.add(10)
                "-" -> {
                }
                else -> formList.add(formString.toInt())
            }
        }
        formList.let { list ->
            var total = 0f
            list.forEach {
                total += it
            }
            total / list.size
        }
    }

    fun refresh() {
        state.postValue(StateHandler.loading())
        intLiveData.postValue(refreshValue)
    }


}
