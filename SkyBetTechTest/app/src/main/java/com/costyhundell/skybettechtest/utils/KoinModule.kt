package com.costyhundell.skybettechtest.utils

import android.content.Context
import com.costyhundell.skybettechtest.Constants
import com.costyhundell.skybettechtest.repositories.RaceDetailsRepository
import com.costyhundell.skybettechtest.repositories.RaceDetailsUseCase
import com.costyhundell.skybettechtest.repositories.RaceMeetingsRepository
import com.costyhundell.skybettechtest.repositories.RaceMeetingsUseCase
import com.costyhundell.skybettechtest.view_models.RaceDetailsViewModel
import com.costyhundell.skybettechtest.view_models.RaceMeetingsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

private val subscriberScheduler = Schedulers.io()
private val observerScheduler = AndroidSchedulers.mainThread()


val viewModelModule = module {
    viewModel { RaceMeetingsViewModel(get()) }
    viewModel { RaceDetailsViewModel(get()) }
}

val repositoryModule = module {
    single { RaceMeetingsUseCase(APIFactory.horseRacesApi, subscriberScheduler, observerScheduler) as RaceMeetingsRepository }
    single { RaceDetailsUseCase(APIFactory.horseRacesApi, subscriberScheduler, observerScheduler, get()) as RaceDetailsRepository }
}

val sharedPreferencesModule = module {
    single { androidApplication().getSharedPreferences(Constants.SHARED_PREFS_FILE, Context.MODE_PRIVATE) }
}