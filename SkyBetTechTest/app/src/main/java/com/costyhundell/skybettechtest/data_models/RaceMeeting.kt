package com.costyhundell.skybettechtest.data_models

import com.google.gson.annotations.SerializedName

data class RaceMeeting(@SerializedName("name")val raceName: String,
                       @SerializedName("course_name") val course: String,
                       val distance: String,
                       val date: String,
                       val time: String,
                       @SerializedName("ride_count") val riders: Int,
                       val going: String,
                       @SerializedName("has_handicap")val handicapped: Boolean,
                       @SerializedName("race_id") val id: Int)
