package com.costyhundell.skybettechtest.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.costyhundell.skybettechtest.R
import com.costyhundell.skybettechtest.data_models.RaceMeeting
import com.costyhundell.skybettechtest.utils.inflate
import com.costyhundell.skybettechtest.view_holders.RaceMeetingViewHolder

class RaceMeetingsAdapter: ListAdapter<RaceMeeting, RaceMeetingViewHolder>(DIFF_UTIL) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RaceMeetingViewHolder = RaceMeetingViewHolder(
        parent.inflate(R.layout.race_meeting_view_holder))

    override fun onBindViewHolder(holder: RaceMeetingViewHolder, position: Int) = holder.bind(getItem(position))

    companion object {
        private val DIFF_UTIL = object: DiffUtil.ItemCallback<RaceMeeting>() {
            override fun areItemsTheSame(oldItem: RaceMeeting, newItem: RaceMeeting): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: RaceMeeting, newItem: RaceMeeting): Boolean {
                return oldItem.handicapped == newItem.handicapped
                        && oldItem.course == newItem.course
                        && oldItem.date == newItem.date
                        && oldItem.distance == newItem.distance
                        && oldItem.going == newItem.going
                        && oldItem.raceName == newItem.raceName
                        && oldItem.riders == newItem.riders
                        && oldItem.time == newItem.time
                        && oldItem.id == newItem.id
            }

        }
    }
}
