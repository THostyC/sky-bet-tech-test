package com.costyhundell.skybettechtest.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.costyhundell.skybettechtest.Constants
import com.costyhundell.skybettechtest.R
import com.costyhundell.skybettechtest.adapters.RaceDetailsAdapter
import com.costyhundell.skybettechtest.utils.StateHandler
import com.costyhundell.skybettechtest.view_models.RaceDetailsViewModel
import kotlinx.android.synthetic.main.race_details_layout.*
import kotlinx.android.synthetic.main.race_details_layout.race_name
import org.koin.androidx.viewmodel.ext.android.viewModel

class RaceDetailsFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private val adapter = RaceDetailsAdapter()
    private val navArgs: RaceDetailsFragmentArgs by navArgs()
    private val viewModel: RaceDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.race_details_layout, null)
        recyclerView = view.findViewById(R.id.race_details_recycler_view)
        swipeRefreshLayout = view.findViewById(R.id.race_details_swipe_refresh)
        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = adapter
        viewModel.getRiders().observe(this, Observer {
            adapter.submitList(it)
        })

        viewModel.getSortedList().observeForever {
            adapter.submitList(it)
        }

        viewModel.getState().observeForever {
            when (it.state) {
                StateHandler.Companion.State.LOADING -> swipeRefreshLayout.isRefreshing = true
                StateHandler.Companion.State.SUCCESS -> swipeRefreshLayout.isRefreshing = false
                // On Error
                else -> {
                    swipeRefreshLayout.isRefreshing = false
                    Toast.makeText(context!!, "Error Loading Data", Toast.LENGTH_SHORT).show()
                }
            }
        }

        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }

        val courseMap = course_map
        viewModel.getRaceDetails().observe(this, Observer { summary ->
            race_name.text = summary.raceName
            race_course.text = summary.course
            race_time.text = summary.time
            distance.text = summary.distance

            Glide.with(view)
                .load(summary.url)
                .placeholder(null)
                .into(courseMap)
        })

        toggle_button.apply {
            isChecked =
                viewModel.getIsFractional()
            setOnClickListener {
                viewModel.setIsFractional(this.isChecked)
                swipeRefreshLayout.isRefreshing = true
                adapter.submitList(emptyList())
                viewModel.loadRiders(navArgs.raceId)
            }
        }

        viewModel.loadRiders(navArgs.raceId)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sort_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sort_number_asc -> {
                viewModel.sortByClothAsc()
            }
            R.id.sort_number_desc -> {
                viewModel.sortByClothDesc()
            }
            R.id.sort_odds_asc -> {
                viewModel.sortByOddsAsc()
            }
            R.id.sort_odds_desc -> {
                viewModel.sortByOddsDesc()
            }
            R.id.sort_form_asc -> {
                viewModel.sortByFormAsc()
            }
            R.id.sort_form_desc -> {
                viewModel.sortByFormDesc()
            }
            R.id.reset -> {
                viewModel.loadRiders(navArgs.raceId)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}