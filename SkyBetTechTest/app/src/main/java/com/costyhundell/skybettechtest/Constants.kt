package com.costyhundell.skybettechtest

object Constants {
   const val SHARED_PREFS_FILE = "PREFERENCES"
   const val SKY_BET_URL = "http://m.skybet.com/horse-racing"
   const val IS_FRACTIONAL_KEY = "isFractional"
}