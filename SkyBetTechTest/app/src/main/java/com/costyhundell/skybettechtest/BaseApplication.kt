package com.costyhundell.skybettechtest

import android.app.Application
import com.costyhundell.skybettechtest.utils.repositoryModule
import com.costyhundell.skybettechtest.utils.sharedPreferencesModule
import com.costyhundell.skybettechtest.utils.viewModelModule
import org.koin.android.ext.android.startKoin

class BaseApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(
            viewModelModule,
            repositoryModule,
            sharedPreferencesModule
        ))
    }
}