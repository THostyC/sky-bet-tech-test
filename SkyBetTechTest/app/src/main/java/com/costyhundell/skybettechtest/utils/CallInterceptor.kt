package com.costyhundell.skybettechtest.utils

import okhttp3.*

class CallInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val responseBody = when {
            chain.request().url().toString().contains("/races") ->
                ResponseBody.create(
                    MediaType.parse("application/json"),
                    raceMeetingsResponse
                )
            else -> ResponseBody.create(
                MediaType.parse("application/json"), when (chain.request().url().pathSegments()[1]) {
                    1.toString() -> race1Response
                    2.toString() -> race2Response
                    3.toString() -> race3Response
                    4.toString() -> race4Response
                    5.toString() -> race5Response
                    6.toString() -> race6Response
                    7.toString() -> race7Response
                    8.toString() -> race8Response
                    9.toString() -> race9Response
                    10.toString() -> race10Response
                    else -> "{}"
                }
            )
        }

        return Response.Builder().code(200)
            .message("Success")
            .protocol(Protocol.HTTP_2)
            .request(chain.request())
            .body(responseBody)
            .build()
    }

}
