package com.costyhundell.skybettechtest.view_holders

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.costyhundell.skybettechtest.Constants
import com.costyhundell.skybettechtest.data_models.Ride
import com.costyhundell.skybettechtest.fragments.RaceDetailsFragmentDirections
import com.costyhundell.skybettechtest.utils.Utils
import kotlinx.android.synthetic.main.rider_info_view_holder.view.*

class RiderInfoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val horseName = itemView.horse_name
    private val odds = itemView.odds
    private val form = itemView.form
    private val clothNumber = itemView.cloth_number

    private val sharedPreferences = itemView.context.getSharedPreferences(Constants.SHARED_PREFS_FILE, Context.MODE_PRIVATE)

    @SuppressLint("SetTextI18n")
    fun bind(item: Ride) {
        horseName.text = item.horseDetails.name
        odds.text = when {
            item.odds == "1/1" -> "EVS"
            sharedPreferences.getBoolean("isFractional", true) -> item.odds
            else -> Utils.convertToDecimal(item.odds).toString()
        }
        form.text = "Form: ${item.form}"
        clothNumber.text = item.number.toString()

        if (item.colour != null) {
            clothNumber.backgroundTintList = ColorStateList.valueOf(Color.parseColor(item.colour))
        }

        odds.setOnClickListener {
            it.findNavController().navigate(RaceDetailsFragmentDirections.openWebView())
        }
    }
}
