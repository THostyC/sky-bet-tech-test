package com.costyhundell.skybettechtest.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.costyhundell.skybettechtest.R
import com.costyhundell.skybettechtest.adapters.RaceMeetingsAdapter
import com.costyhundell.skybettechtest.utils.StateHandler
import com.costyhundell.skybettechtest.view_models.RaceMeetingsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RaceMeetingsFragment: Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private val adapter = RaceMeetingsAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.race_meetings_layout, null)

        recyclerView = view.findViewById(R.id.race_meetings_recycler_view)
        swipeRefresh = view.findViewById(R.id.race_meetings_swipe_refresh)

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel: RaceMeetingsViewModel by viewModel()
        recyclerView.adapter = adapter
        swipeRefresh.setOnRefreshListener {
            viewModel.refresh()
        }
        viewModel.getRaces().observeForever {
            adapter.submitList(it)
        }

        viewModel.getState().observeForever {
            when (it.state) {
                StateHandler.Companion.State.LOADING -> swipeRefresh.isRefreshing = true
                StateHandler.Companion.State.SUCCESS -> swipeRefresh.isRefreshing = false
                // On Error
                else -> {
                    swipeRefresh.isRefreshing = false
                    Toast.makeText(context!!, "Error Loading Data", Toast.LENGTH_SHORT).show()
                }
            }
        }

        viewModel.loadRaceResponse()
    }
}