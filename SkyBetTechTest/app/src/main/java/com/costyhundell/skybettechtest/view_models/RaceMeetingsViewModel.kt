package com.costyhundell.skybettechtest.view_models

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.costyhundell.skybettechtest.repositories.RaceMeetingsRepository
import com.costyhundell.skybettechtest.data_models.RaceMeeting
import com.costyhundell.skybettechtest.utils.StateHandler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

class RaceMeetingsViewModel(raceMeetingRepository: RaceMeetingsRepository) : ViewModel(), LifecycleOwner {
    override fun getLifecycle(): LifecycleRegistry = LifecycleRegistry(this)


    @VisibleForTesting
    val raceResponse = raceMeetingRepository.getRaceList()

    private val races = MutableLiveData<List<RaceMeeting>>()


    private val state = MutableLiveData<StateHandler>()
    private val compositeDisposable = CompositeDisposable()

    fun refresh() {
        state.postValue(StateHandler.loading())
        loadRaceResponse()
    }

    fun loadRaceResponse() {
        raceResponse.subscribe { response ->
            if (response != null) {
                state.postValue(StateHandler.success())
                races.postValue(response)
            } else {
                state.postValue(StateHandler.error(Throwable("No data")))
            }
        }.let {
            compositeDisposable.add(it)
        }
    }

    fun getRaces(): LiveData<List<RaceMeeting>> = races
    fun getState(): LiveData<StateHandler> = state

    override fun onCleared() {
        super.onCleared()
        state.removeObservers(this)
        races.removeObservers(this)
        compositeDisposable.dispose()
    }
}
