package com.costyhundell.skybettechtest.repositories

import com.costyhundell.skybettechtest.data_models.RaceMeeting
import io.reactivex.Single

interface RaceMeetingsRepository {
    fun getRaceList() : Single<List<RaceMeeting>>
}