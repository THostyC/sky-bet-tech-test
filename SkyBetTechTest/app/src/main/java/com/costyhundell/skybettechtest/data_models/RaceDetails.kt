package com.costyhundell.skybettechtest.data_models

import com.google.gson.annotations.SerializedName
import java.math.RoundingMode
import java.text.DecimalFormat

data class RaceDetails(
    @SerializedName("race_summary") val summary: RaceSummary,
    val rides: List<Ride>
)

data class RaceSummary(
    @SerializedName("name") val raceName: String,
    @SerializedName("course_name") val course: String,
    val distance: String,
    val date: String,
    val time: String,
    @SerializedName("ride_count") val riders: Int,
    @SerializedName("race_stage") val stage: String,
    val going: String,
    @SerializedName("has_handicap") val handicapped: Boolean,
    val hidden: Boolean,
    @SerializedName("course_url") val url: String
)

data class Ride(
    @SerializedName("cloth_number") val number: Int,
    @SerializedName("cloth_colour") val colour: String?,
    @SerializedName("horse") val horseDetails: HorseDetails,
    @SerializedName("formsummary") val form: String,
    val handicap: String,
    @SerializedName("current_odds") var odds: String
)

data class HorseDetails(
    val name: String,
    @SerializedName("days_since_last_run") val lastRace: Int,
    val foaled: String,
    val sex: String
)