package com.costyhundell.skybettechtest.view_holders

import android.annotation.SuppressLint
import android.view.View
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.costyhundell.skybettechtest.data_models.RaceMeeting
import com.costyhundell.skybettechtest.fragments.RaceMeetingsFragmentDirections
import kotlinx.android.synthetic.main.race_meeting_view_holder.view.*

class RaceMeetingViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val raceName = itemView.race_name
    private val courseName = itemView.course_name
    private val runners = itemView.runners
    private val going = itemView.going
    private val startTime = itemView.start_time

    @SuppressLint("SetTextI18n")
    fun bind(item: RaceMeeting) {
        raceName.text = item.raceName
        courseName.text = "Course: ${item.course}"
        runners.text = "${item.riders} Runners"
        going.text = item.going
        startTime.text = item.time

        itemView.setOnClickListener {
            val action = RaceMeetingsFragmentDirections.openRace()
            action.raceId = item.id
            it.findNavController().navigate(action)
        }
    }
}
