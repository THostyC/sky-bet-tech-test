package com.costyhundell.skybettechtest.repositories

import com.costyhundell.skybettechtest.data_models.RaceDetails
import io.reactivex.Single

interface RaceDetailsRepository {
    fun getRaceDetails(id: Int): Single<RaceDetails>

    fun getIsFractional(): Boolean

    fun setIsFractional(isFractional: Boolean)
}
