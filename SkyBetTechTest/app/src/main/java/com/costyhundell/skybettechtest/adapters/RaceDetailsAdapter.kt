package com.costyhundell.skybettechtest.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.costyhundell.skybettechtest.R
import com.costyhundell.skybettechtest.data_models.Ride
import com.costyhundell.skybettechtest.utils.inflate
import com.costyhundell.skybettechtest.view_holders.RiderInfoViewHolder

class RaceDetailsAdapter: ListAdapter<Ride, RiderInfoViewHolder>(DIFF_UTIL) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RiderInfoViewHolder
            = RiderInfoViewHolder(parent.inflate(R.layout.rider_info_view_holder))

    override fun onBindViewHolder(holder: RiderInfoViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DIFF_UTIL = object: DiffUtil.ItemCallback<Ride>() {
            override fun areItemsTheSame(oldItem: Ride, newItem: Ride): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Ride, newItem: Ride): Boolean {
                return oldItem.form == newItem.form
                        && oldItem.handicap == newItem.handicap
                        && oldItem.horseDetails == newItem.horseDetails
                        && oldItem.number == newItem.number
                        && oldItem.odds == newItem.odds
            }

        }
    }

}
