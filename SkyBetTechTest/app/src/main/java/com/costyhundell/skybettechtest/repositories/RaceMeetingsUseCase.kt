package com.costyhundell.skybettechtest.repositories

import com.costyhundell.skybettechtest.data_models.RaceMeeting
import com.costyhundell.skybettechtest.utils.HorseRacesApi
import io.reactivex.Scheduler
import io.reactivex.Single

class RaceMeetingsUseCase(private val apiService: HorseRacesApi, val subscriberScheduler: Scheduler, val observerScheduler: Scheduler): RaceMeetingsRepository {
    override fun getRaceList(): Single<List<RaceMeeting>> =
        apiService.getRacesList().subscribeOn(subscriberScheduler).observeOn(observerScheduler)
}