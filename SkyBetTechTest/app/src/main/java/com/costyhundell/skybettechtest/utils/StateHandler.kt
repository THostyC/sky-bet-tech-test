package com.costyhundell.skybettechtest.utils


class StateHandler (val state: State, val error: Throwable?) {
    companion object {
        enum class State { LOADING, SUCCESS, ERROR }

        fun success(): StateHandler = StateHandler(State.SUCCESS, null)
        fun error(error: Throwable):  StateHandler = StateHandler(State.ERROR, error)
        fun loading():  StateHandler = StateHandler(State.LOADING, null)
    }
}