package com.costyhundell.skybettechtest.repositories

import android.content.SharedPreferences
import android.provider.SyncStateContract
import com.costyhundell.skybettechtest.Constants
import com.costyhundell.skybettechtest.data_models.RaceDetails
import com.costyhundell.skybettechtest.utils.HorseRacesApi
import io.reactivex.Scheduler
import io.reactivex.Single

class RaceDetailsUseCase(private val apiService: HorseRacesApi, val subscriberScheduler: Scheduler, val observerScheduler: Scheduler, val sharedPreferences: SharedPreferences) : RaceDetailsRepository {
    override fun getRaceDetails(id: Int): Single<RaceDetails>
            = apiService.getRaceDetails(id).subscribeOn(subscriberScheduler).observeOn(observerScheduler)

    override fun getIsFractional(): Boolean = sharedPreferences.getBoolean(Constants.IS_FRACTIONAL_KEY, true)

    override fun setIsFractional(isFractional: Boolean) = sharedPreferences.edit().putBoolean(Constants.IS_FRACTIONAL_KEY, isFractional).apply()
}