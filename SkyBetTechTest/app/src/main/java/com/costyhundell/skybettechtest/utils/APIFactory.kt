package com.costyhundell.skybettechtest.utils

import com.costyhundell.skybettechtest.data_models.RaceDetails
import com.costyhundell.skybettechtest.data_models.RaceMeeting
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

object APIFactory {

    private val interceptor = CallInterceptor()

    private val client = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    val horseRacesApi: HorseRacesApi = Retrofit.Builder()
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("http://test.com/")
        .build()
        .create(HorseRacesApi::class.java)
}

interface HorseRacesApi {
    @GET("races")
    fun getRacesList(): Single<List<RaceMeeting>>

    @GET("race/{id}")
    fun getRaceDetails(@Path("id") id: Int): Single<RaceDetails>

}