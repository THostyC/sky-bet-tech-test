package com.costyhundell.skybettechtest.utils

import java.math.RoundingMode
import java.text.DecimalFormat

object Utils {
    fun convertToDecimal(odds:String) : Float = odds.split("/").let { splitString ->
        val oddsList = mutableListOf<Float>()
        splitString.forEach { number ->
            oddsList.add(number.toFloat())
        }
        val oddsFloat = (oddsList[0] / oddsList[1] + 1)
        // Trims the odds to 2 decimal places
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        df.format(oddsFloat).toFloat()
    }
}