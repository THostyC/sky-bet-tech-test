# Sky Bet Tech Test
---

## Languages

I chose to use Kotlin as my primary language and RxJava was used for making network calls so that they are done on a different thread.

## Libraries and Dependencies

I used Retrofit for the network layer as this is a widely accepted networking library. It also has a good mocking framework which allows for nice and easy Unit test creation. For image caching I used Glide because I was pulling the images from a server instead of storing in app so that they could be dynamic.
I then used Koin for dependency injection, this was because not only is Koin written in Kotlin but is also incredibly lightweight and easy to use. I feel it simple to understand what it is doing, and how it is doing DI.

## Architecture

The architecture of the code is MVVM. All logic is done inside the ViewModel, with the Model or `UseCase` in this instance being where any data retrieval or storage is done. The UseCases are extended by using a Repository interface to again allow for easy testing when using Koin to inject a view we simply mock the interface. The View layer then simply listens for data updates from the ViewModel and then displays the data to the user.